/**
 * Apex Class Name: ContentDocumentLinkTriggerHandler
 * Description: Trigger handler for ContentDocumentLinkTrigger trigger
 * Created By: Wasef 01/10/2019
 */
public without sharing class ContentDocumentLinkTriggerHandler {

    /*
     * Created By: Wasef 01/10/2019
     * Parameters: List<ContentDocumentLink>
     * Return Type: void
     * Description: Method to handle before insert event on the ContentDocumentLink object
    */
    public static void onBeforeInsert(List<ContentDocumentLink> newList){
        
        shareContentDocumentLink(newList);
        
    }
    /*
     * Created By: Wasef 01/10/2020
     * Parameters: List<ContentDocumentLink>
     * Return Type: void
     * Description: Method to share documents uploaded by internal user with external/all user.
    */
    public static void shareContentDocumentLink(List<ContentDocumentLink> newList) {
        String keyPrefix = ObjectFromWhichFilesNeedToBeShared__c.getSobjectType().getDescribe().getKeyPrefix();
		
        for(ContentDocumentLink documentLink : newList) {
			//Inferred Permission
            if(String.valueOf(documentLink.LinkedEntityId).startsWithIgnoreCase(keyPrifix) ) {
                documentLink.shareType = 'I';
                documentLink.Visibility = 'AllUsers';
            }
			//Viewer Permission
			else if(String.valueOf(documentLink.LinkedEntityId).startsWithIgnoreCase(keyPrifix)){
				documentLink.shareType = 'V';
                documentLink.Visibility = 'AllUsers';
			}
			//Collaborator Permission
			else if(String.valueOf(documentLink.LinkedEntityId).startsWithIgnoreCase(keyPrifix)){
				documentLink.shareType = 'C';
                documentLink.Visibility = 'AllUsers';
			}
        }
    }
}

